#!/bin/bash

docker stop jcasc-demo
docker rm jcasc-demo
docker rmi automatingguy/jcasc-demo
docker volume rm jenkins_home-jcasc
